package camt.se234.lab11.service;

import camt.se234.lab11.dao.StudentDao;
import camt.se234.lab11.dao.StudentDaoImpl;
import camt.se234.lab11.entity.Student;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class StudentServiceImplTest {
    StudentDao studentDao;
    StudentServiceImpl studentService;

    @Test
    public void testFindById(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33)));
        assertThat(studentService.findStudentById("234"),is(new Student("234","B","temp",4.00)));
        assertThat(studentService.findStudentById("345"),is(new Student("345","C","temp",3.25)));
        assertThat(studentService.findStudentById("456"),is(new Student("456","D","temp",2.60)));
        assertThat(studentService.findStudentById("567"),is(new Student("567","E","temp",1.75)));
    }

    @Test
    public void getAverageGpaTest(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getNewAverageGpa(),is(2.474));
    }

    @Test
    public void testWithMock(){
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33)));
    }

    @Test
    public void testFindById2(){
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("234","B","temp",4.00));
        mockStudents.add(new Student("345","C","temp",3.25));
        mockStudents.add(new Student("456","D","temp",2.60));
        mockStudents.add(new Student("567","E","temp",1.75));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33)));
    }

    @Test
    public void getAverageGpaTestByMock(){
       //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents= new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("234","B","temp",4.00));
        mockStudents.add(new Student("345","C","temp",3.25));
        mockStudents.add(new Student("456","D","temp",2.60));
        mockStudents.add(new Student("567","E","temp",1.75));
        when(studentDao.findAllNew()).thenReturn(mockStudents);
        assertThat(studentService.getNewAverageGpa(),is(2.786));
    }
    
    @Test
    public void testFindByPartOfId(){
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents = new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("124","B","temp",2.33));
        mockStudents.add(new Student("223","C","temp",2.33));
        mockStudents.add(new Student("224","D","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("22"),hasItem(new Student("223","C","temp",2.33)));
        assertThat(studentService.findStudentByPartOfId("22"),hasItems(new Student("223","C","temp",2.33),new Student("224","D","temp",2.33)));
    }

    @Test
    public void testFindByPartOfId2(){
        //StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents = new ArrayList<>();
        //StudentServiceImpl studentService = new StudentServiceImpl();
        //studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123","A","temp",2.33));
        mockStudents.add(new Student("234","B","temp",4.00));
        mockStudents.add(new Student("345","C","temp",3.25));
        mockStudents.add(new Student("456","D","temp",2.60));
        mockStudents.add(new Student("567","E","temp",1.75));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("23"),hasItem(new Student("123","A","temp",2.33)));
        assertThat(studentService.findStudentByPartOfId("23"),hasItems(new Student("123","A","temp",2.33),new Student("234","B","temp",4.00)));
    }

    @Before
    public void setup(){
        studentDao = mock(StudentDao.class);
        studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
    }

    @Test(expected = NoDataException.class)
    public void testNoDataException(){
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("55"),nullValue());
    }

    @Test(expected = ArraySizeZeroException.class)
    public void testArraySizeZeroException(){
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123","A","temp",2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("0"),is(0));
    }

    @Test(expected = ArithmeticException.class)
    public void testArithmeticException(){
        List<Student> mockStudents = new ArrayList<>();
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),nullValue());
    }
}
