package camt.se234.lab11.service;


import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(JUnitParamsRunner.class)
public class GradeServiceImplTest {
    @Test
    public void testGetGrade(){
        GradeServiceImpl gradeService = new GradeServiceImpl();
        assertThat(gradeService.getGradeMidAndFinal(50,50),is("A"));
        assertThat(gradeService.getGradeMidAndFinal(50,30),is("A"));
        assertThat(gradeService.getGradeMidAndFinal(40,38.9),is("B"));
        assertThat(gradeService.getGradeMidAndFinal(40,35),is("B"));
        assertThat(gradeService.getGradeMidAndFinal(40,34.5),is("C"));
        assertThat(gradeService.getGradeMidAndFinal(30,30),is("C"));
        assertThat(gradeService.getGradeMidAndFinal(20,39.4),is("D"));
        assertThat(gradeService.getGradeMidAndFinal(18,15),is("D"));
        assertThat(gradeService.getGradeMidAndFinal(13.3,18.7),is("F"));
        assertThat(gradeService.getGradeMidAndFinal(0,0),is("F"));
    }

    public Object paramsForTestGetGradeParams(){
        return new Object[][]{
                {50,50,"A"},
                {45,32,"B"},
                {30,32,"C"},
                {20,35,"D"},
                {0,0,"F"}
        };
    }

    @Test
    @Parameters(method = "paramsForTestGetGradeParams")
    @TestCaseName("Test getGrade Params [{index}] : input is {0}, expect \"{1}\"")
    public void testGetGradeparams(double midtermScore, double finalScore,String expectedGrade){
        GradeServiceImpl gradeService=new GradeServiceImpl();
        assertThat(gradeService.getGradeMidAndFinal(midtermScore, finalScore),is(expectedGrade));
    }
}
