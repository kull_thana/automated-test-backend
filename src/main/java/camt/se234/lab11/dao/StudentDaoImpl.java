package camt.se234.lab11.dao;

import camt.se234.lab11.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDao {
    List<Student> students;
    List<Student> newStudents;
    public StudentDaoImpl(){
        students = new ArrayList<>();
        students.add(new Student("123","A","temp",2.33));
        students.add(new Student("234","B","temp",4.00));
        students.add(new Student("345","C","temp",3.25));
        students.add(new Student("456","D","temp",2.60));
        students.add(new Student("567","E","temp",1.75));

        newStudents=new ArrayList<>();
        newStudents.add(new Student("678","F","temp",3.33));
        newStudents.add(new Student("789","G","temp",1.00));
        newStudents.add(new Student("910","H","temp",3.79));
        newStudents.add(new Student("101","I","temp",1.75));
        newStudents.add(new Student("102","J","temp",2.50));
    }

    @Override
    public List<Student> findAll() {
        return this.students;
    }

    @Override
    public List<Student> findAllNew() {
        return this.newStudents;
    }
}
